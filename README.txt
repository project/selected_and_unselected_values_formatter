The default formatter for multiple values displays only those values that were
selected in the node edit form. Sometimes it's useful to show all values and
with some added markup indicate which values were selected.

This module provides a formatter that shows both unselected and selected values
and lets you define markup to be printed before and after the value itself. It
wraps the item in a span and lets you define the css class for that span,
different one for the selected and unselected value.

I18n translations will be used if the corresponding modules (Field translation,
Taxonomy translation) are enabled and fields are actually translated.

Caution: no sanitation will be done on entered values on settings page.


Possible use cases
------------------

You need to express what the total space of choices are.

[ ] A (superior)
[ ] A-
[ ] B+
[X] B (good)
[ ] B-
[ ] C+
[ ] C (average)
[ ] C-
[ ] D+
[ ] D (poor)
[ ] F (failure)
[ ] I (incomplete)
[ ] PR (course in progress)
[ ] W (withdrew from course)
[ ] FF (failing in a pass/fail course)
[ ] PP (passing in a pass/fail course)
[ ] N/C (not for credit)


You need to express your progress on a checklist.

[X] AV needs sent to hotel
[X] Flowers for the head table ordered
[X] Room set-up forms sent to the hotel
[X] Head table seating determined
[X] Meal programs prepared
[ ] Meal programs sent to caterer
[ ] Evaluation forms for conference prepared
[ ] Reminders to speakers sent


You need to be able to visually compare nodes (for example with 
https://drupal.org/project/views_flipped_table).

Room one       Room two       Room three
[x] Shower     [x] Shower     [x] Shower
[x] Ironing    [ ] Ironing    [ ] Ironing
[ ] Mini bar   [ ] Mini bar   [x] Mini bar
[ ] View       [x] View       [ ] View
[x] Extra bed  [ ] Extra bed  [ ] Extra bed


Style examples
---------------

Whatever markup you like, before or after the value itself.

[x] Selected
[ ] Unselected

[x] Selected
[_] Unselected

x Selected
  Unselected

001 [x]
002 [ ]

(some image) Selected
(other image) Unselected


How to use
----------

Configuration is at
admin/config/content/selected_and_unselected_values

The field formatter will be available in the formatter list for some list
types. For example for Basic page at 
admin/structure/types/manage/page/display and also in Views Configure Field 
dialog.
